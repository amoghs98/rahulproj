﻿namespace GetEstimates
{
    using System;
    using System.Collections.Generic;

    using System.Linq;
    using System.Web;
    public class Estimate
    {

        public int MonthlyInstallment { get; set; }
        public int TotalPremium{get; set;}
        public int TotalCoverage { get; set; }
        public int BuildYear { get; set; }
    }
}