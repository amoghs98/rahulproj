using GetEstimates;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class Tests
    {
        IEstimateMethod Estimate;
        [SetUp]
        public void Setup()
        {
            Estimate = new EstimateMethod();
        }

        [Test]
        public void Test1()
        {
            var actualOutput = Estimate.GetEstimates(2012).MonthlyInstallment;

            //Assert
            Assert.AreEqual(1000, actualOutput);
        }

        [Test]
        public void Test2()
        {
            var actualOutput = Estimate.GetEstimates(2015).MonthlyInstallment;

            //Assert
            Assert.AreEqual(1200, actualOutput);
        }

        [Test]
        public void Test3()
        {
            var actualOutput = Estimate.GetEstimates(2019);

            //Assert
            Assert.AreEqual(null, actualOutput);
        }
    }
}