﻿
namespace JSONParser
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Web.Mvc;

    public interface IDeserializer
    {
        List<SelectListItem> Deserialize(string postCode);
    }
}
