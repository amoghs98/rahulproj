﻿namespace JSONParser
{
    using JSONParser.Models;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Web.Mvc;

    public class Deserializer : IDeserializer
    {
        
        public List<SelectListItem> Deserialize(string postCode)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (File.Exists("D:/Project/JSONParser/JsonData/" + postCode + ".json")) { 
                FileStream stream = new FileStream("D:/Project/JSONParser/JsonData/"+postCode+".json", FileMode.Open);
                StreamReader reader = new StreamReader(stream);

                string json = reader.ReadToEnd();
                PostCode model = (PostCode)JsonConvert.DeserializeObject(json, typeof(PostCode));
                List<Addresses> address = model.PostAddresses;
                foreach (var item in address)
                {
                    items.Add(new SelectListItem
                    {
                        Text = item.Address,
                        Value = item.Address
                    }) ;
                }
                reader.Close();
                stream.Close();
                return items;
            } else
            {
                return null;
            }
        }
    }
}
