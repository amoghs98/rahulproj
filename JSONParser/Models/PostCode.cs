﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JSONParser.Models
{
    public class PostCode
    {
        public List<Addresses> PostAddresses { get; set; }
        public PostCode()
        {
            PostAddresses = new List<Addresses>();
        }
    }
}
