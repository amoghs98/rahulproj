﻿using System;
using System.Web.Mvc;
using GetEstimates;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MotorInsurance.Areas.India.Controllers;
using MotorInsurance.Areas.India.Models;
using MvcContrib.TestHelper;

namespace MotorInsuranceWeb.test
{
    [TestClass]
    public class Tests
    {

        TestControllerBuilder builder = new TestControllerBuilder();
        DetailsController details = new DetailsController();
        DisplayController display = new DisplayController();
        GetEstimates.Estimate e = new GetEstimates.Estimate { BuildYear = 2012, MonthlyInstallment = 1200};
        EstimateController estimate; 

        [TestMethod]
        public void DetailsControllerToPersonView()
        {
            builder.InitializeController(details);
            var result = details.PersonDetails() as ViewResult;
            Assert.AreEqual(new Person().GetType(), result.Model.GetType()) ;
        }

        [TestMethod]
        public void DetailsControllerToVehicleView()
        {
            builder.InitializeController(details);
            Person model = new Person { Name = "Test", Email = "Test@gmail.com" };
            var result = details.VehicleDetails(model) as ViewResult;
            Assert.AreEqual(new Vehicle().GetType(), result.Model.GetType());
        }

        [TestMethod]
        public void DisplayControllerTDisplayView()
        {
            builder.InitializeController(display);
            var result = display.Display() as ViewResult;
            Assert.AreEqual(new DisplayModel().GetType(), result.Model.GetType());
        }

        [TestMethod]
        public void EstimateControllerToEstimateView()
        {

            estimate = new EstimateController(new EstimateMethod());
            builder.InitializeController(estimate);
            var vehicleModel = new Vehicle { BuildYear = 2012, VehicleNumber = "KA02fdsw" };
            var result = estimate.GetEstimate(vehicleModel) as ViewResult;
            Assert.AreEqual(result.ViewName,"");
        }
    }
}
