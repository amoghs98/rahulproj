using JSONParser;
using JSONParser.Models;
using NUnit.Framework;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Tests
{
    public class Tests
    {
        IDeserializer deserializer;
        [SetUp]
        public void Setup()
        {
            deserializer = new Deserializer();
        }

        [TestCase("WC2N 5DU", 3)]
        [TestCase("W1J 6NS", 2)]
        public void Test1(string postcode,int count)
        {
            var model = (List<SelectListItem>)deserializer.Deserialize(postcode);
            Assert.AreEqual(model.Count,count);
        }

        [TestCase(null, null)]
        public void NullTest(string a, string b)
        {
            var model = (List<SelectListItem>)deserializer.Deserialize(a);
            Assert.AreEqual(b, model);
        }

        [TestCase("PostCode4", null)]
        public void ReferenceNotFoundTest(string a, string b)
        {
            var model = (List<SelectListItem>)deserializer.Deserialize(a);
            Assert.AreEqual(b, model);
        }
    }
}