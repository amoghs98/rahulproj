﻿namespace MotorInsurance.Areas.India.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;

    public class Vehicle
    {
        [Display(Name = "Vehicle Number")]
        [Required(ErrorMessage = "Please Enter Vehicle Number")]
        public string VehicleNumber { get; set; }

        [Display(Name = "Engine Number")]
        [Required(ErrorMessage = "Please Enter Engine Number")]
        [RegularExpression("^([A-Za-z]{2}[A-z0-9]{5,16})$", ErrorMessage = "Entered engine number is not valid.")]
        public string EngineNumber { get; set; }

        [Display(Name = "Travelled Kilometers")]
        [Required(ErrorMessage = "Please enter travelled Kilometers")]
        [Range(0, 300000)]
        public int KmsTravelled { get; set; }

        [Display(Name = "Build year")]
        [Required(ErrorMessage = "Please enter build year")]
        [Range(1999, 2020)]
        public int BuildYear { get; set; }

    }
}