﻿namespace MotorInsurance.Areas.India.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;

    public class Person
    {
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please Enter name")]
        public string Name { get; set; }

        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Please Enter Correct DOB")]
        public DateTime DateOfBirth { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Enter valid Email Address"), DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Phone Number")]
        [Required(ErrorMessage = "Enter valid Valid Phone Number"), DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }


        [Display(Name = "Postal Code")]
        [Required(ErrorMessage = "Postal Code Required!")]
        [StringLength(6, ErrorMessage = "Enter Correct PinCode")]
        
        public string PostCode { get; set; }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Pick an Address"), DataType(DataType.Text)]
        public string Address { get; set; }
    }
}