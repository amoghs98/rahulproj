﻿namespace MotorInsurance.Areas.India.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public class EstimateList
    {
        public List<Estimate> EList { get; set; }

        public EstimateList()
        {
            EList = new List<Estimate>();
        }
    }
}