﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotorInsurance.Areas.India.Models
{
    public class PostCode
    {
        public List<Addresses> PostAddresses { get; set; }
        public PostCode()
        {
            PostAddresses = new List<Addresses>();
        }
    }
}