﻿namespace MotorInsurance.Areas.India.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;

    public class Estimate
    {
        [Display(Name = "Monthly Installment")]
        public int MonthlyInstallment { get; set; }

        [Display(Name = "Total Premium")]
        public int TotalPremium { get; set; }

        [Display(Name = "Total Coverage")]
        public int TotalCoverage { get; set; }

        [Display(Name = "Build Year")]
        public int BuildYear { get; set; }

        public static explicit operator Estimate(GetEstimates.Estimate v)
        {
            Estimate model = new Estimate
            {
                BuildYear = v.BuildYear,
                MonthlyInstallment = v.MonthlyInstallment,
                TotalCoverage = v.TotalCoverage,
                TotalPremium = v.TotalPremium
            };
            return model;
        }
    }
}