﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MotorInsurance.Areas.India.Models
{
    public class DisplayModel
    {
       
            public Person PersonDetails { get; set; }

            public Vehicle VehicleDetails { get; set; }

            public Estimate EstimateDetails { get; set; }

        
        
    }
}