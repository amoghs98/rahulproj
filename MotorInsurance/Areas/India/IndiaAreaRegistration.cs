﻿namespace MotorInsurance.Areas.India
{
    using System.Web.Mvc;

    public class IndiaAreaRegistration : AreaRegistration{
        /// <inheritdoc/>
        public override string AreaName
        {
            get
            {
                return "India";
            }
        }

        /// <inheritdoc/>
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "India_default",
                "India/{controller}/{action}/{id}",
                new { action = "PersonDetails", id = UrlParameter.Optional }
                );
        }
    }
}