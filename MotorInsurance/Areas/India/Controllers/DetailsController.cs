﻿namespace MotorInsurance.Areas.India.Controllers
{
    using MotorInsurance.Areas.India.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class DetailsController : Controller
    {
        // GET: India/Details
        public ActionResult PersonDetails()
        {
            
            var personModel = (Person)Session["person"];
            if (personModel == null)
            {
                personModel = new Person();
            }
            return View(personModel);
        }

        public ActionResult VehicleDetails(Person personModel)
        {
            if (personModel != null && personModel.Email != null)
            {
                Session["person"] = personModel;
            }
            var vehicleModel = Session["vehicle"];
            if (vehicleModel == null)
            {
                vehicleModel = new Vehicle();
            }
            return View(vehicleModel);
        }

    }
}