﻿namespace MotorInsurance.Areas.India.Controllers
{
    using MotorInsurance.Areas.India.Models;
    using System.Web.Mvc;

    public class DisplayController : Controller
    {
        // GET: UK/Display
        public ActionResult Display()
        {
            var DisplayVar = new DisplayModel();
            DisplayVar.PersonDetails = (Person)Session["person"];
            DisplayVar.VehicleDetails = (Vehicle)Session["vehicle"];
            DisplayVar.EstimateDetails = (Estimate)Session["quote"];
            Session.RemoveAll();
            return View(DisplayVar);
        }
    }
}