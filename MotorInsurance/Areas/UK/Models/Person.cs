﻿namespace MotorInsurance.Areas.UK.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;

    public class Person
    {
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please Enter name")]
        public string Name { get; set; }

        [Display(Name = "Date of Birth")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Please Enter Correct DOB")]
        public DateTime DateOfBirth { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Enter valid Email Address"), DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Phone Number")]
        [Required(ErrorMessage = "Enter valid Valid Phone Number"), DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }


        [Display(Name = "Postal Code")]
        [Required(ErrorMessage = "Postal Code Required!")]
        [RegularExpression("^([A-Za-z][A-Ha-hK-Yk-y]?[0-9][A-Za-z0-9]? [0-9][A-Za-z]{2}|[Gg][Ii][Rr] 0[Aa]{2})$", ErrorMessage = "Entered postcode format is not valid.")]
        public string PostCode { get; set; }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Pick an Address"), DataType(DataType.Text)]
        public string Address { get; set; }
    }
}