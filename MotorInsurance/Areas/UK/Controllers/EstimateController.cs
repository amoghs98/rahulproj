﻿
namespace MotorInsurance.Areas.UK.Controllers
{
    using GetEstimates;
    using MotorInsurance.Areas.UK.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class EstimateController : Controller
    {
        private IEstimateMethod _Estimate;

        public EstimateController(IEstimateMethod estimateMethod)
        {
            _Estimate = estimateMethod;
        }


        // GET: UK/Estimate
        public ActionResult GetEstimate(Vehicle vehicleModel)
        {
            if (vehicleModel != null)
            {
                Session["vehicle"] = vehicleModel;
            }
            Models.Estimate model = (Models.Estimate)_Estimate.GetEstimates(vehicleModel.BuildYear);

            if (model != null)
            {
                Session["quote"] = model;
                return View(model);
               
            }
            return RedirectToAction("VehicleDetails", "Details", vehicleModel);

        }
    }
}