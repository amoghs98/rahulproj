﻿namespace MotorInsurance.Areas.UK.Controllers
{
    using MotorInsurance.Areas.UK.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class DisplayController : Controller
    {
        // GET: UK/Display
        public ActionResult Display()
        {
            var DisplayVar = new DisplayModel();
            DisplayVar.PersonDetails = (Person)Session["person"];
            DisplayVar.VehicleDetails = (Vehicle)Session["vehicle"];
            DisplayVar.EstimateDetails = (Estimate)Session["quote"];
            Session.RemoveAll();
            return View(DisplayVar);
        }
    }
}