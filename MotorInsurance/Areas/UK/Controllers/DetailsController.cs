﻿namespace MotorInsurance.Areas.UK.Controllers
{
    using JSONParser.Models;
    using MotorInsurance.Areas.UK.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class DetailsController : Controller
    {
        // GET: UK/Details
        public ActionResult PersonDetails()
        {
            var personModel = (Person)Session["person"];
            if (personModel == null)
            {
                personModel = new Person();
            }
            return View(personModel);
        }

        public ActionResult VehicleDetails(Person personModel)
        {
            if (personModel != null && personModel.Email != null)
            {
                Session["person"] = personModel;
            }
            var vehicleModel = Session["vehicle"];
            if (vehicleModel == null)
            {
                vehicleModel = new Vehicle();
            }
            return View(vehicleModel);
        }

        public ActionResult GetAddress(string postCode)
        {
            PostCode model = new PostCode();
            return PartialView(model);
        }

    }
}