﻿namespace MotorInsurance.Areas.UK.GetEstimate
{
    using MotorInsurance.Areas.UK.Models;
    public interface IEstimateMethod
    {
       Estimate GetEstimates(int buildYear);
    }
}
