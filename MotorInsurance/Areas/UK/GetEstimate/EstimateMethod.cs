﻿using MotorInsurance.Areas.UK.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace MotorInsurance.Areas.UK.GetEstimate
{
    public class EstimateMethod : IEstimateMethod
    {
        public Estimate GetEstimates(int buildYear)
        {
            if (File.Exists("D:/Project/MotorInsurance/Areas/UK/GetEstimate/EstimateList.json"))
            {
                FileStream stream = new FileStream("D:/Project/MotorInsurance/Areas/UK/GetEstimate/EstimateList.json", FileMode.Open);
                StreamReader reader = new StreamReader(stream);

                string json = reader.ReadToEnd();
                EstimateList model = (EstimateList)JsonConvert.DeserializeObject(json, typeof(EstimateList));
                reader.Close();
                stream.Close();
                foreach (var item in model.EList)
                {
                    if (buildYear == item.BuildYear)
                    {
                        return item;
                    }
                }
                return null;
            }
            else
            {
                return null;
            }
        }
    }
}