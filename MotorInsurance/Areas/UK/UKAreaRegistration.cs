﻿using System.Web.Mvc;

namespace MotorInsurance.Areas.UK
{
    public class UKAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "UK";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "UK_default",
                "UK/{controller}/{action}/{id}",
                new { action = "PersonDetails", id = UrlParameter.Optional }
            );
        }
    }
}