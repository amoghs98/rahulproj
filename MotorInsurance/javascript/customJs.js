﻿function checkInputForUk() {

    //alert("");
    //var list = ["WC2N 5DU", "W1J 6NS"];
    
        var postVar = document.getElementById("PostCode").value;
    //alert(postVar);
    if (postVar == "WC2N 5DU" || postVar == "W1J 6NS") {
        //alert(postVar);

        document.getElementById("AddressDiv").style.visibility = "visible";
        $.ajax({

            type: "POST",
            url: "/Home/GetAddress",
            datatype: "Json",
            data: { PostCode: $('#PostCode').val() },
            success: function (data) {
                $.each(data, function (index, address) {

                    $('#Address').append('<option value="' + address.Value + '">' + address.Text + '</option>');
                });
            },
            error: function () {
                
                alert('error');
            }

        });
    }
    else {
        document.getElementById("AddressDiv").style.visibility = "hidden";
    }
}

function checkInputForIndia() {

    //alert("");
    //var list = ["WC2N 5DU", "W1J 6NS"];

    var postVar = document.getElementById("PostCode").value;
    //alert(postVar);
    if (postVar == "560056" || postVar == "560075") {
        //alert(postVar);

        document.getElementById("AddressDiv").style.visibility = "visible";
        $.ajax({

            type: "POST",
            url: "/Home/GetAddress",
            datatype: "Json",
            data: { PostCode: $('#PostCode').val() },
            success: function (data) {
                $.each(data, function (index, address) {

                    $('#Address').append('<option value="' + address.Value + '">' + address.Text + '</option>');
                });
            },
            error: function () {

                alert('error');
            }

        });
    }
    else {
        document.getElementById("AddressDiv").style.visibility = "hidden";
    }
}

