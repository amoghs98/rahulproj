﻿using JSONParser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MotorInsurance.Controllers
{
    public class HomeController : Controller
    {
        private IDeserializer _deserializer;

        public HomeController(IDeserializer deserializer)
        {
            _deserializer = deserializer;
        }
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetAddress(string PostCode)
        {
            var item = _deserializer.Deserialize(PostCode);
            return Json(item);
        }
    }
}